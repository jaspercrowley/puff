# Change Log
## [Unreleased]

## [0.0.8]
- Color changes to list hover and invalid text

## [0.0.7]
- More workbench and editor color updates

## [0.0.6]
- Added more workbench colors
- New icon
- Higher quality preview image

## [0.0.5]
- Added workbench colors

## [0.0.4]
- Added preview image

## [0.0.3]
- Renamed theme

## [0.0.2]
- Added an icon

## [0.0.1]